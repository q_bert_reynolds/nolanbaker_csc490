using UnityEngine;
using UnityEditor; // make sure you import this package
using System.Collections;

public class GroupSelectedObjects {
	[MenuItem("Utility/Group Selection _g")]
    static void GroupSelection () {
		GameObject[] selected = Selection.gameObjects;
		Transform parent = selected[0].transform.parent;
		
		GameObject newGroup = new GameObject("group");
		Undo.RegisterCreatedObjectUndo(newGroup, "Undo Create Group");
		
		Undo.RegisterSetTransformParentUndo(newGroup.transform, parent, "Blah");
		newGroup.transform.parent = parent;
		
		foreach (GameObject go in selected) {
			Undo.RegisterSetTransformParentUndo(go.transform, newGroup.transform, "Blah");
			go.transform.parent = newGroup.transform;
		}
	}  
	
	[MenuItem("Utility/Group Selection _g", true)]
	static bool ValidateGroupSelection () {
		GameObject[] selected = Selection.gameObjects;
		if (selected.Length > 0) {
			Transform parent = selected[0].transform.parent;
			foreach (GameObject go in selected) {
				if (parent != go.transform.parent) {
					return false;	
				}
			}
			return true;
		}
		return false;
	}
}
