using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour {
	
	public float maxPosChange = 0.1f;
	public float maxRotChange = 1;
	
	private bool isShaking = false;
	private float duration = 0;
	private float timeLeft = 0;
	
	private Vector3 initialPos;
	private Quaternion initialRot;
	
	public void Start () {
		initialPos = transform.position;
		initialRot = transform.rotation;
	}
	
	public void Shake (float duration) {
		this.duration = duration;
		timeLeft = duration;
		
		if (isShaking) return; // exit after resetting values if coroutine is already running
		else StartCoroutine(ShakeCoroutine());
	}
	
	private IEnumerator ShakeCoroutine () {
		isShaking = true;
		
		while (timeLeft > 0) {
			transform.position = initialPos;
			transform.rotation = initialRot;
			float magnitude = timeLeft / duration;
			
			transform.position += new Vector3(Random.Range(-maxPosChange, maxPosChange),
				Random.Range(-maxPosChange, maxPosChange),
				Random.Range(-maxPosChange, maxPosChange)) * magnitude;
			
			transform.Rotate(new Vector3(Random.Range(-maxRotChange, maxRotChange),
				Random.Range(-maxRotChange, maxRotChange),
				Random.Range(-maxRotChange, maxRotChange)) * magnitude);
			
			timeLeft -= Time.deltaTime;
			yield return null;
		}
		isShaking = false;
	}
}
