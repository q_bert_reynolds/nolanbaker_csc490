using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class FollowTarget : MonoBehaviour {
	
	public Transform target;
	public float speed = 3f;
	
	void Update () {
		if (target) {
			if (Application.isPlaying)
				transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * speed);
			else
				transform.position = target.position;
		}
	}
}
