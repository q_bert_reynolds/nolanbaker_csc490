using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[ExecuteInEditMode]
public class Spring : MonoBehaviour {
	
	public Transform target;

	public float compressedLength = 2f;
	public float radius = 1f;
	public float crossSectionRadius = 0.1f;
	public int segmentsPerStrip = 10;
	public int numStrips = 6;
	
	private float length = 3f;

	private float numCoils {
		get { return compressedLength / (2.0f * crossSectionRadius * radius); }
	}
	
	private float unwoundLength {
		get { return numCoils * 2.0f * Mathf.PI * radius; }		
	}
	
	void Update () {
		if (target) {
			length = Vector3.Distance(transform.position, target.position);
			UpdateMesh();
		}
	}
	
	[ContextMenu("Update Mesh")]
	void UpdateMesh () {	
	
		Vector3[] verts = new Vector3[numStrips * segmentsPerStrip];
		Vector3[] norms = new Vector3[numStrips * segmentsPerStrip];
		Vector2[] uvs = new Vector2[numStrips * segmentsPerStrip];
		List<int> tris = new List<int>();
		
		for (int strip = 0; strip < numStrips; strip++) {
			for (int segment = 0; segment < segmentsPerStrip; segment++) {
				int index = strip * segmentsPerStrip + segment;
				
				float t = (float)segment / (float)segmentsPerStrip;
				float cos_r = Mathf.Cos((float)strip * Mathf.PI * 2.0f / (float)numStrips);
				float sin_r = Mathf.Sin((float)strip * Mathf.PI * 2.0f / (float)numStrips);
				float sin_R = Mathf.Sin(unwoundLength * t);
				float cos_R = Mathf.Cos(unwoundLength * t);
				float r = radius + crossSectionRadius * cos_r;
				float h = length * t + crossSectionRadius * sin_r;
				verts[index] = new Vector3(r * cos_R, h, r * sin_R);
				
				norms[index] = new Vector3(cos_r * cos_R, sin_r, cos_r * sin_R);
				
				float u = (float)strip / (float)numStrips;
				float v = (float)segment / (float)segmentsPerStrip;
				uvs[index] = new Vector2(u, v);
					
				if (segment < segmentsPerStrip - 1) {
					tris.AddRange(new int[6]{
						strip * segmentsPerStrip + segment,
						((strip + 1) % numStrips) * segmentsPerStrip + segment,
						strip * segmentsPerStrip + segment + 1,
						strip * segmentsPerStrip + segment + 1,
						((strip + 1) % numStrips) * segmentsPerStrip + segment,
						((strip + 1) % numStrips) * segmentsPerStrip + segment + 1,
					});
				}
			}
		}
		
		
		
		Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
		if (!mesh) {
			mesh = new Mesh();
			GetComponent<MeshFilter>().sharedMesh = mesh;
		}
		mesh.vertices = verts;
		mesh.normals = norms;
		mesh.uv = uvs;
		mesh.triangles = tris.ToArray();
	}
}
