using UnityEngine;
using System.Collections;

public class PongBall : MonoBehaviour {
	
	public float speedIncrement = 1;
	public float initialSpeed = 5;
	
	public PongScore leftScore;
	public PongScore rightScore;
	
	private float speed = 1;
	
	void Start () {
		speed = initialSpeed;
		rigidbody.velocity = Vector3.right * speed;	
	}
	
	void Update () {
		Vector3 v = rigidbody.velocity;
		Vector3 topRight = Camera.main.WorldToViewportPoint(transform.position + collider.bounds.extents);
		Vector3 bottomLeft = Camera.main.WorldToViewportPoint(transform.position - collider.bounds.extents);
	
		// ball hits top or bottom of screen
		if (topRight.y > 1) {
			rigidbody.velocity = new Vector3(v.x, -Mathf.Abs(v.y), v.z);
			audio.Play();
		}
		else if (bottomLeft.y < 0) {
			rigidbody.velocity = new Vector3(v.x, Mathf.Abs(v.y), v.z);
			audio.Play();
		}
		
		// ball goes off sides of screen
		else if (bottomLeft.x > 1) {
			leftScore.IncreaseScore();
			Reset(1);
		}
		else if (topRight.x < 0) {
			rightScore.IncreaseScore();
			Reset(-1);
		}
	}
	
	void Reset (float dir) {
		speed = initialSpeed;
		transform.position = Vector3.zero;
		rigidbody.velocity = Vector3.right * dir * initialSpeed;
	}

	void OnCollisionExit (Collision collision) {
		speed += speedIncrement;
		rigidbody.velocity = rigidbody.velocity.normalized * speed;	
	}
}
