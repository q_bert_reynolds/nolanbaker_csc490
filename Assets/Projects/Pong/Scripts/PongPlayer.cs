using UnityEngine;
using System.Collections;

public class PongPlayer : MonoBehaviour {
	
	public float speed = 10;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		rigidbody.velocity = Vector3.up * Input.GetAxis("Vertical") * speed;
	}
}
