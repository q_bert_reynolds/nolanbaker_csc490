using UnityEngine;
using System.Collections;

public class PongNPC : MonoBehaviour {
	
	PongBall ball;
	
	public float speed = 5;
	public float acceleration = 10;
	public float minDist = 2;
	
	float steering = 0;
	
	// Use this for initialization
	void Start () {
		ball = (PongBall)FindObjectOfType(typeof(PongBall));
	}
	
	// Update is called once per frame
	void Update () {
		float newSteering = 0;
		if (transform.position.y - minDist > ball.transform.position.y)
			newSteering = -1;
		else if (transform.position.y + minDist < ball.transform.position.y)
			newSteering = 1;
		
		steering = Mathf.Lerp(steering, newSteering, Time.deltaTime * acceleration);	
		
		rigidbody.velocity = Vector3.up * steering * speed;
	}
}
