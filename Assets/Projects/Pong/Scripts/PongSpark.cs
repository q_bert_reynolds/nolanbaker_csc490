using UnityEngine;
using System.Collections;

public class PongSpark : MonoBehaviour {

	public void Explode (Collision collision) {
		transform.position = collision.contacts[0].point;
		
		// collision callback is on paddle, so normal points toward paddle
		transform.LookAt(transform.position - collision.contacts[0].normal);
		
		particleSystem.Clear();
		particleSystem.Play();
	}
}
