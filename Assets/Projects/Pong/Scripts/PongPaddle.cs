using UnityEngine;
using System.Collections;

public class PongPaddle : MonoBehaviour {
	
	public PongSpark spark;
	private CameraShake shake;
	
	void Start () {
		shake = (CameraShake)Camera.main.GetComponent(typeof(CameraShake));	
	}
	
	void OnCollisionEnter (Collision collision) {
		PongBall ball = collision.gameObject.GetComponent(typeof(PongBall)) as PongBall;
		if (ball != null) {
			animation.Play();
			spark.Explode(collision);
			shake.Shake(0.2f);
			audio.Play();
		}
	}
}
