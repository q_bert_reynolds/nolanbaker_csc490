using UnityEngine;
using System.Collections;

public class PongScore : MonoBehaviour {

	private float score = 0;
	private TextMesh textMesh;
	
	public void Start () {
		textMesh = (TextMesh)GetComponent(typeof(TextMesh));	
	}
	
	public void IncreaseScore () {
		score++;
		textMesh.text = score.ToString();
		animation.Play();
		audio.Play();
	}	
}
