using UnityEngine;
using System.Collections;
 
[RequireComponent (typeof (Rigidbody))]
[RequireComponent (typeof (CapsuleCollider))]
public class CharacterMotor : MonoBehaviour {
 
	public float speed = 1.0f;
	public float maxSpeedChange = 1.0f;
	public float jumpHeight = 1.0f;
	public float airMovement = 0.1f;
	
	[HideInInspector]
	public bool shouldJump = false;
	[HideInInspector]
	public Vector3 targetVelocity = Vector3.zero;
	
	bool grounded = false;
	
	void FixedUpdate () {
	    Vector3 velocity = rigidbody.velocity;
        Vector3 velocityChange = (targetVelocity - velocity);
		velocityChange = velocityChange.normalized * Mathf.Clamp(velocityChange.magnitude, -maxSpeedChange, maxSpeedChange);
        velocityChange.y = 0;
 
		if (!grounded)
			velocityChange = velocityChange * airMovement;	
		else if (shouldJump)
			rigidbody.velocity = new Vector3(velocity.x, CalculateJumpVerticalSpeed(), velocity.z);
		
        rigidbody.AddForce(velocityChange, ForceMode.VelocityChange);
		
	    grounded = false;
	}
	
	void OnCollisionStay (Collision collision) {
		foreach (ContactPoint cp in collision.contacts) {
			if (cp.normal.y > Mathf.Abs(cp.normal.x) && cp.normal.y > Mathf.Abs(cp.normal.z)) {
				grounded = true;
			}
		}
	}
 
	float CalculateJumpVerticalSpeed () {
	    return Mathf.Sqrt(2 * jumpHeight * -Physics.gravity.y);
	}
}